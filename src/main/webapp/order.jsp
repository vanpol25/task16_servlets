<%@ page import="com.polahniuk.model.dto.PizzaDto" %>
<%@ page import="com.polahniuk.StaticResources" %>
<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Orders</title>
</head>

<body>
<h2>Orders:</h2>
<table border="1">
    <tr>
        <th>Id</th>
        <th>First name</th>
        <th>Second name</th>
        <th>Pizza</th>
        <th>Price</th>
        <th>Dough</th>
        <th>Toppings</th>
        <th>Sauces</th>
    </tr>
    <% Map<Long, PizzaDto> pizzas = StaticResources.getPizzas(); %>
    <%
        for (Long id : pizzas.keySet()) {
            PizzaDto pizza = pizzas.get(id);
    %>
    <tr>
        <td>
            <%= pizza.getId() %>
        </td>
        <td>
            <%= pizza.getFirstName() %>
        </td>
        <td>
            <%= pizza.getSecondName() %>
        </td>
        <td>
            <%= pizza.getPizzaType() %>
        </td>
        <td>
            <%= pizza.getPrice() %>
        </td>
        <td>
            <%= pizza.getDough() %>
        </td>
        <td>
            <%= pizza.getToppings() %>
        </td>
        <td>
            <%= pizza.getSauces() %>
        </td>
    </tr>
    <%}%>
</table>
</body>
</html>
