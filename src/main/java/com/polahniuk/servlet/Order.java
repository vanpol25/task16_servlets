package com.polahniuk.servlet;

import com.polahniuk.StaticResources;
import com.polahniuk.model.pizza.Pizza;
import com.polahniuk.model.pizza.PizzaType;
import com.polahniuk.model.pizza.factory.PizzaFactory;
import com.polahniuk.model.pizza.factory.impl.PizzaFactoryImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/orders/*")
public class Order extends HttpServlet {

    private Logger log = LogManager.getLogger(Order.class);

    @Override
    public void init() throws ServletException {
        log.info("Servlet " + this.getServletName() + " has stared.");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Into doGet()");
        req.setAttribute("name", "Ivan");
        req.getRequestDispatcher("order.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Into doPost()");
        PizzaFactory factory = new PizzaFactoryImpl();
        Pizza pizza;
        String firstName = req.getParameter("firstName");
        String secondName = req.getParameter("secondName");
        try {
            PizzaType pizzaType = PizzaType.valueOf(req.getParameter("pizza"));
            pizza = factory.bakePizza(pizzaType, firstName, secondName);
            StaticResources.addPizza(pizza);
        } catch (IllegalArgumentException e) {
            log.info(e);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Into doDelete()");
        String id = req.getParameter("id");
        StaticResources.removePizza(Long.valueOf(id));
    }

    @Override
    public void destroy() {
        log.info("Servlet " + this.getServletName() + " has stopped.");
    }
}
