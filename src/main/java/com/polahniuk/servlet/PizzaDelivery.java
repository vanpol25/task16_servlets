package com.polahniuk.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/pizza")
public class PizzaDelivery extends HttpServlet {

    private static Logger log = LogManager.getLogger(PizzaDelivery.class);

    @Override
    public void init() throws ServletException {
        log.info("Servlet " + this.getServletName() + " has stared.");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Into doGet()");
        req.setAttribute("name", "Ivan");
        req.getRequestDispatcher("main.jsp").forward(req, resp);
    }

    @Override
    public void destroy() {
        log.info("Servlet " + this.getServletName() + " has stopped.");
    }
}
