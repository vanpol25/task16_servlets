package com.polahniuk.model.dto;

import com.polahniuk.model.pizza.Pizza;
import com.polahniuk.model.pizza.recipe.Recipe;
import com.polahniuk.model.pizza.recipe.Sauce;
import com.polahniuk.model.pizza.recipe.Topping;
import lombok.Data;

import java.util.List;

@Data
public class PizzaDto {

    private Long id;
    private String firstName;
    private String secondName;
    private String pizzaType;
    private String dough;
    private String toppings;
    private String sauces;
    private int price;


    public PizzaDto(Pizza pizza) {
        Recipe recipe = pizza.getRecipe();
        id = pizza.getId();
        firstName = pizza.getFirstName();
        secondName = pizza.getSecondName();
        pizzaType = pizza.getType().name();
        dough = recipe.getDough().name();
        setToppings(recipe);
        setSauces(recipe);
        price = pizza.getPrice();
    }

    private void setToppings(Recipe recipe) {
        List<Topping> toppings = recipe.getToppings();
        String toppingsStr = "";
        for (int i = 0; i < toppings.size(); i++) {
            if (i == toppings.size() - 1) {
                toppingsStr += toppings.get(i) + ".";
            } else {
                toppingsStr += toppings.get(i) + ", ";
            }
        }
        this.toppings = toppingsStr;
    }

    private void setSauces(Recipe recipe) {
        List<Sauce> sauces = recipe.getSauces();
        String saucesStr = "";
        for (int i = 0; i < sauces.size(); i++) {
            if (i == sauces.size() - 1) {
                saucesStr += sauces.get(i) + ".";
            } else {
                saucesStr += sauces.get(i) + ", ";
            }
        }
        this.sauces = saucesStr;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", pizzaType='" + pizzaType + '\'' +
                ", dough='" + dough + '\'' +
                ", toppings='" + toppings + '\'' +
                ", sauces='" + sauces + '\'' +
                ", price=" + price +
                '}';
    }
}
