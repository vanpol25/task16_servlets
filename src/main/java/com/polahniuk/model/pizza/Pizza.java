package com.polahniuk.model.pizza;

import com.polahniuk.model.pizza.recipe.Recipe;

public interface Pizza {

    Recipe getRecipe();

    int getPrice();

    PizzaType getType();

    long getId();

    String getFirstName();

    String getSecondName();

    void prepare();

    void bake();

    void cut();

    void box();

}
