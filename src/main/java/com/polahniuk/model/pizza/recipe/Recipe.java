package com.polahniuk.model.pizza.recipe;

import java.util.List;

public class Recipe {

    private Dough dough;
    private List<Sauce> sauces;
    private List<Topping> toppings;

    public void setDough(Dough dough) {
        this.dough = dough;
    }

    public void setSauces(List<Sauce> sauces) {
        this.sauces = sauces;
    }

    public void setToppings(List<Topping> toppings) {
        this.toppings = toppings;
    }

    public Dough getDough() {
        return dough;
    }

    public List<Sauce> getSauces() {
        return sauces;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "dough=" + dough +
                ", sauces=" + sauces +
                ", toppings=" + toppings +
                '}';
    }
}
