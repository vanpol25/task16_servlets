package com.polahniuk.model.pizza.recipe;

public enum Dough {
    ThickCrust, ThinCrust
}
