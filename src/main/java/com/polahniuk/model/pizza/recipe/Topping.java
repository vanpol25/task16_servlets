package com.polahniuk.model.pizza.recipe;

public enum Topping {
    Pepperoni,
    Mushrooms,
    Onions,
    Sausage,
    Bacon,
    ExtraCheese,
    BlackOlives,
    GreenPeppers,
    Pineapple,
    Spinach
}
