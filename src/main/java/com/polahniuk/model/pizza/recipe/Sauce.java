package com.polahniuk.model.pizza.recipe;

public enum Sauce {
    Marinara,
    PlumTomato,
    Pesto,
    Bechamel,
    BBQ,
    Pumpkin,
    Beet
}
