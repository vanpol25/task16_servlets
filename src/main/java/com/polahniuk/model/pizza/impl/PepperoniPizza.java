package com.polahniuk.model.pizza.impl;

import com.polahniuk.StaticResources;
import com.polahniuk.model.pizza.Pizza;
import com.polahniuk.model.pizza.PizzaType;
import com.polahniuk.model.pizza.recipe.Dough;
import com.polahniuk.model.pizza.recipe.Recipe;
import com.polahniuk.model.pizza.recipe.Sauce;
import com.polahniuk.model.pizza.recipe.Topping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class PepperoniPizza implements Pizza {

    private final Long id;
    private String firstName;
    private String secondName;
    private Logger log = LogManager.getLogger(PepperoniPizza.class);
    private Recipe recipe;
    private PizzaType type = PizzaType.Pepperoni;
    private Dough dough;
    private List<Sauce> sauces;
    private List<Topping> toppings;
    private final int price = 140;

    public PepperoniPizza(String firstName, String secondName) {
        this.id = StaticResources.getIdCounter();
        this.firstName = firstName;
        this.secondName = secondName;
    }

    @Override
    public Recipe getRecipe() {
        return recipe;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public PizzaType getType() {
        return type;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getSecondName() {
        return secondName;
    }

    @Override
    public void prepare() {
        recipe = new Recipe();
        dough = Dough.ThinCrust;
        sauces = Arrays.asList(Sauce.PlumTomato, Sauce.Pesto, Sauce.Marinara);
        toppings = Arrays.asList(Topping.GreenPeppers,
                Topping.Mushrooms,
                Topping.Pepperoni,
                Topping.Bacon,
                Topping.BlackOlives);
        log.info(type + " prepared.");
    }

    @Override
    public void cut() {
        recipe.setDough(dough);
        recipe.setSauces(sauces);
        recipe.setToppings(toppings);
        log.info(type + " cuted.");
    }

    @Override
    public void bake() {
        log.info(type + " baked.");
    }

    @Override
    public void box() {
        log.info(type + " boxed.");
    }

    @Override
    public String toString() {
        return "PepperoniPizza{" +
                "recipe=" + recipe +
                '}';
    }
}
