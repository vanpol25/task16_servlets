package com.polahniuk.model.pizza.factory;

import com.polahniuk.model.pizza.Pizza;
import com.polahniuk.model.pizza.PizzaType;

public abstract class PizzaFactory {

    protected abstract Pizza createPizza(PizzaType type, String firstName, String secondName);

    public Pizza bakePizza(PizzaType type, String firstName, String secondName) {
        Pizza pizza = createPizza(type, firstName, secondName);
        pizza.prepare();
        pizza.cut();
        pizza.bake();
        pizza.box();
        return pizza;
    }

}
