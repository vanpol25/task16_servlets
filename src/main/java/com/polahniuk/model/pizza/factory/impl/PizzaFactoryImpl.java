package com.polahniuk.model.pizza.factory.impl;

import com.polahniuk.model.pizza.Pizza;
import com.polahniuk.model.pizza.PizzaType;
import com.polahniuk.model.pizza.factory.PizzaFactory;
import com.polahniuk.model.pizza.impl.CheesePizza;
import com.polahniuk.model.pizza.impl.ClamPizza;
import com.polahniuk.model.pizza.impl.PepperoniPizza;
import com.polahniuk.model.pizza.impl.VeggiePizza;

public class PizzaFactoryImpl extends PizzaFactory {

    @Override
    protected Pizza createPizza(PizzaType type, String firstName, String secondName) {
        Pizza pizza;
        if (type == PizzaType.Cheese) {
            pizza = new CheesePizza(firstName,secondName);
        } else if (type == PizzaType.Veggie) {
            pizza = new VeggiePizza(firstName,secondName);
        } else if (type == PizzaType.Pepperoni) {
            pizza = new PepperoniPizza(firstName,secondName);
        } else if (type == PizzaType.Clam) {
            pizza = new ClamPizza(firstName,secondName);
        } else {
            pizza = null;
        }
        return pizza;
    }

}
