package com.polahniuk.model.pizza;

public enum PizzaType {
    Cheese, Veggie, Clam, Pepperoni
}
