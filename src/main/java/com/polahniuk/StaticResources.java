package com.polahniuk;

import com.polahniuk.model.dto.PizzaDto;
import com.polahniuk.model.pizza.Pizza;
import com.polahniuk.model.pizza.PizzaType;
import com.polahniuk.model.pizza.factory.PizzaFactory;
import com.polahniuk.model.pizza.factory.impl.PizzaFactoryImpl;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class StaticResources {

    private static Long idCounter = 0L;
    private static SortedMap<Long, PizzaDto> pizzas;

    static {
        pizzas = new TreeMap<>();
        PizzaFactory factory = new PizzaFactoryImpl();
        Pizza pizza1 = factory.bakePizza(PizzaType.Cheese, "Ivan", "Polahniuk");
        Pizza pizza2 = factory.bakePizza(PizzaType.Pepperoni, "Ivan", "Polahniuk");
        Pizza pizza3 = factory.bakePizza(PizzaType.Clam, "Ivan", "Polahniuk");
        pizzas.put(pizza1.getId(), new PizzaDto(pizza1));
        pizzas.put(pizza2.getId(), new PizzaDto(pizza2));
        pizzas.put(pizza3.getId(), new PizzaDto(pizza3));
    }

    public static Map<Long, PizzaDto> getPizzas() {
        return pizzas;
    }

    public static void addPizza(Pizza pizza) {
        pizzas.put(pizza.getId(), new PizzaDto(pizza));
    }

    public static void removePizza(Long id) {
        pizzas.remove(id);
    }

    public static void putPizza(Long id, Pizza pizza) {
        pizzas.put(id, new PizzaDto(pizza));
    }

    public synchronized static long getIdCounter() {
        return ++idCounter;
    }
}
